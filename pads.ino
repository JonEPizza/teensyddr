/*
  Pair of Dance Pads

  Sends keyboard key presses when buttons are depressed.  Usable for input wherever key presses
  are accepted.

  @JonEPizza

  The required Bounce2 library can be fetched with the Arduino IDE package manager.
  You also must set the USB type to include Keyboard under Tools > USB
*/

#include <Bounce2.h>

// The Teensy pins each button is physically wired to
const int padOneL = 14;
const int padOneU = 15;
const int padOneD = 16;
const int padOneR = 17;

const int padTwoL = 19;
const int padTwoU = 20;
const int padTwoD = 21;
const int padTwoR = 22;

int padPins[] = {padOneL, padOneU, padOneD, padOneR, padTwoL, padTwoU, padTwoD, padTwoR};
char padCharacters[] = {'a', 'w', 'r', 's', 'n', 'u', 'e', 'i'};

// We will use the included Bounce2 library to check the state of each button pin.  This library
// only reports a change of state on the pin after the pin has stayed in that state for at least
// debounce interval.  This is important because contacts actually physically bounce away from
// each other on first contact which a computer could detect as you releasing the button when
// just pressing it down.
Bounce debouncedReaders[] = {Bounce(), Bounce(), Bounce(), Bounce(),
                             Bounce(), Bounce(), Bounce(), Bounce()
                            };
const int debounceInterval = 5; // in ms

bool stateChanged[] = {false, false, false, false, false, false, false, false};

void setup()
{
  // For every pin with a wired button:
  for (int i = 0; i < 8; i++)
  {
    // 1. Set that pin's mode to INPUT_PULLUP in which the voltage on the pin is made to return to
    //    +5v whenever ungrounded, so we can be sure a read of 0v means a button press.
    pinMode(padPins[i], INPUT_PULLUP);
    // 2. Attach a debounced reader to the pin.  Must be done in setup() as the pin must be set
    //    to INPUT_PULLUP prior to attaching debounced reader.
    debouncedReaders[i].attach(padPins[i]);
    debouncedReaders[i].interval(debounceInterval);
  }

  // start the Keyboard library for the purposes of sending key presses and releases
  Keyboard.begin();
}

void loop()
{
  for (int i = 0; i < 8; i++)
  {
    // this is intentionally in its own loop, we want our reads to occur as close as
    // possible to each other, everything should behave as though on one cycle
    stateChanged[i] = debouncedReaders[i].update();
  }

  for (int i = 0; i < 8; i++)
  {
    if (stateChanged[i])
    {
      if (debouncedReaders[i].fell())
      {
        Keyboard.press(padCharacters[i]);
      }
      else
      {
        Keyboard.release(padCharacters[i]);
      }
    }
  }
}
